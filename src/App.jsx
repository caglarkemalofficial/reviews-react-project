import React from 'react'
import User from './User'
const App = () => {
  return (
    <main>
      <User />
    </main>
  )
}
export default App
