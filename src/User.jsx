import { useState, useEffect } from 'react'
import data from './data'

const User = () => {
  const [index, setIndex] = useState(0)
  const nextButtonFunc = () => {
    if (index === data.length - 1) {
      setIndex(0)
    } else {
      setIndex(index + 1)
    }
  }
  const prevButtonFunc = () => {
    if (index === 0) {
      setIndex(data.length - 1)
    } else {
      setIndex(index - 1)
    }
  }
  const randomNumber = () => {
    let newIndex = Math.floor(Math.random() * 4)
    setIndex(newIndex)
  }
  const { name, job, image, text, id } = data[index]
  return (
    <article className="review" key={id}>
      <div className="img-container">
        <img src={image} alt={name} className="person-img" />
        <span className="quote-icon">
          <span>svg gelecek</span>
        </span>
      </div>
      <h4 className="author">{name}</h4>
      <p className="job">{job}</p>
      <p className="info">{text}</p>
      <div className="btn-container">
        <button className="prev-btn" onClick={prevButtonFunc}>
          <span>prev</span>
        </button>
        <button className="next-btn" onClick={nextButtonFunc}>
          <span>next</span>
        </button>
      </div>
      <button className="btn btn-hipster" onClick={randomNumber}>
        surprise me
      </button>
    </article>
  )
}
export default User
